//
//  WebViewController.swift
//  SampleSlydepayIntegration
//
//  Created by Tonte Owuso on 2/19/18.
//  Copyright © 2018 Slydepay. All rights reserved.
//

import UIKit
import WebKit
import Alamofire

class WebViewController: UIViewController,WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var url = ""
        var orderCode = ""
       var payToken = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.navigationDelegate = self
        
        let urlRequest = URL(string: self.url)
        let request = URLRequest(url: urlRequest!)
        webView.load(request)
        
        
        // Do any additional setup after loading the view.
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Swift.Void){
        decisionHandler(.allow)
        let urlString = webView.url?.absoluteString
        if urlString!.contains(Globals.callbackUrl){
            webView.isHidden = true
            webView.stopLoading()
            activityIndicator.isHidden = false
            Alamofire.request(
                URL(string: "https://app.slydepay.com.gh/api/merchant/invoice/checkstatus")!,
                method: .post,
                parameters: ["emailOrMobileNumber": Globals.emailOrMobileNumber,"merchantKey": Globals.merchantKey, "orderCode":orderCode,"payToken":payToken,"confirmTransaction":true],
                encoding: JSONEncoding.default)
                .validate()
                .responseString{ (response: DataResponse<String>) in
                    print(response)
                 
                    
                }
                .responseJSON { (response) -> Void in
                    guard response.result.isSuccess else {
                        return
                    }
                    guard let value = response.result.value as? [String: Any],
                        let success =  value["success"] as? Bool else {return}
                    
                    if success {
                        if let result = value ["result"] as? String{
                            if result == "CONFIRMED"{
                                self.openSuccessScreen()
                            }
                            else if result == "PENDING"{
                                
                            }
                            else if result == "DISPUTED"{
                                
                            }
                            else if result == "CANCELLED"{
                                
                            }
                            else if result == "NEW"{
                                
                            }
                            
                        }
                        
                    }
                    else{
                        let errorMessage = value["errorMessage"] as? String
                        if errorMessage != nil{
                            self.displayError(error:errorMessage!)
                        }
                    }
            }
        }
        else{
             activityIndicator.isHidden = false
        }
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.isHidden = true
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.webView.stopLoading()
        self.dismiss(animated: true, completion: nil)
    }
    func displayError(error:String){
        activityIndicator.isHidden = true
        let refreshAlert = UIAlertController(title: "Error", message: error, preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(refreshAlert, animated: true, completion: nil)
    }
    func openSuccessScreen(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let successViewController = storyboard.instantiateViewController(withIdentifier: "success") as? SuccessViewController
        self.present(successViewController!, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
