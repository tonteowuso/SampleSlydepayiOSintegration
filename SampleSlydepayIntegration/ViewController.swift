//
//  ViewController.swift
//  SampleSlydepayIntegration
//
//  Created by Tonte Owuso on 2/19/18.
//  Copyright © 2018 Slydepay. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var amountTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.tapped))
         self.view.addGestureRecognizer(singleTap)
//        activityIndicator.isHidden = true
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func purchaseButtonClicked(_ sender: Any) {
        if amountTextField.text?.isEmpty == false{
            submit()
        }
        else {
            amountTextField.becomeFirstResponder()
        }
    }
    
    
    func submit(){
        activityIndicator.isHidden = false
        let amount = amountTextField.text!
        let emailOrMobileNumber = Globals.emailOrMobileNumber
        let merchantKey = Globals.merchantKey
        let orderCode = NSUUID().uuidString
        
        Alamofire.request(
            URL(string: "https://app.slydepay.com.gh/api/merchant/invoice/create")!,
            method: .post,
            parameters: ["emailOrMobileNumber": emailOrMobileNumber,"merchantKey": merchantKey, "amount": amount, "orderCode":orderCode],
             encoding: JSONEncoding.default)
            .validate()
            .responseString{ (response: DataResponse<String>) in
                print(response)
                
            }
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    self.displayError(error:"Please check your internet connection and try again")
                    self.activityIndicator.isHidden = true
                    return
                }
                guard let value = response.result.value as? [String: Any],
                let success =  value["success"] as? Bool else {
                    return}
                
                if success {
                    if let result = value ["result"] as? [String: Any]{
                        let payToken = result["payToken"] as? String
                        let paymentCode = result["paymentCode"] as? String
                        let orderCode = result["orderCode"] as? String
                        let url = "https://app.slydepay.com/paylive/detailsnew.aspx?pay_token=" + payToken!
                        self.openWebView(url: url, orderCode: orderCode!, payToken: payToken!)
                     }
                    
                }
                else{
                    let errorMessage = value["errorMessage"] as? String
                    if errorMessage != nil{
                         self.displayError(error:errorMessage!)
                    }
                }
            }
        
       
    }

    
    @objc func tapped(){
        amountTextField.resignFirstResponder()
    }
    
    func openWebView(url:String,orderCode:String,payToken:String){
        activityIndicator.isHidden = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let webViewController = storyboard.instantiateViewController(withIdentifier: "web") as? WebViewController
        webViewController?.url = url
        webViewController?.orderCode = orderCode
        webViewController?.payToken = payToken
        self.present(webViewController!, animated: true, completion: nil)
        
    }
    func displayError(error:String){
        activityIndicator.isHidden = true
        let refreshAlert = UIAlertController(title: "Error", message: error, preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(refreshAlert, animated: true, completion: nil)
    }
   
   

}

