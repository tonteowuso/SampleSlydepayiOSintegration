//
//  Globals.swift
//  SampleSlydepayIntegration
//
//  Created by Tonte Owuso on 2/20/18.
//  Copyright © 2018 Slydepay. All rights reserved.
//

import Foundation
public struct Globals {
   
    static let merchantKey = "XXXXXXX"
    static let emailOrMobileNumber = "XXXXXXXXXXXXX"
     static let callbackUrl = "XXXXXXXXXXXX"
}
